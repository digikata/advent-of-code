
d06:
	#cargo test -- --nocapture day05::t::fn_parse_line_kind
	#cargo test -- --nocapture day05::t::ans_test_input
	cargo test -- --nocapture day06

d05:
	#cargo test -- --nocapture day05::t::fn_parse_line_kind
	#cargo test -- --nocapture day05::t::ans_test_input
	cargo test -- --nocapture day05

d04:
	#cargo test -- --nocapture day04::t::fn_parse_line
	cargo test -- --nocapture day04

d03:
	# cargo test -- --nocapture day03::t::ans_test_input
	cargo test -- --nocapture day03

d02:
	cargo test -- --nocapture day02

d01:
	cargo test -- --nocapture day01

test:
	cargo test -- --nocapture

watch:
	cargo watch -s "just"
