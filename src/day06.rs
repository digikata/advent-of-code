//! day06.rs

// use crate::load_buf_lines;

pub fn day06(input: &[String]) -> usize {
   for line in input {
      let nmrk = find_packet(line);
      // println!("day06 packet at {nmrk}");
      return nmrk;
   }
   0usize
}

pub fn day06_p2(input: &[String]) -> usize {
   for line in input {
      let nmrk = find_message(line);
      // println!("day06 message at {nmrk}");
      return nmrk;
   }
   0usize
}

fn find_packet(line: &str) -> usize {
   for (i, win) in line.as_bytes().windows(4).enumerate() {
      if is_start_of_packet(win) {
         return i + 4;
      }
   }
   0usize
}

fn is_start_of_packet(tok: &[u8]) -> bool {
   use std::collections::HashSet;
   let h = HashSet::<u8>::from_iter(tok.iter().take(4).map(|v| *v));
   h.len() == 4
}

// could easily refactor this to supply the N distinct
fn find_message(line: &str) -> usize {
   const N: usize = 14;
   for (i, win) in line.as_bytes().windows(N).enumerate() {
      if is_start_of_message(win) {
         return i + N;
      }
   }
   0usize
}

fn is_start_of_message(tok: &[u8]) -> bool {
   use std::collections::HashSet;
   const N: usize = 14;
   let h = HashSet::<u8>::from_iter(tok.iter().take(N).map(|v| *v));
   h.len() == N
}

#[cfg(test)]
mod t {
   use crate::load_buf_lines;
   use super::*;

   #[test]
   fn ans_test_input_p2() {
      let test_inputs = vec![
         (19, "mjqjpqmgbljsphdztnvjfqwrcgsmlb"),
         (23, "bvwbjplbgvbhsrlpgdmjqwftvncz"),
         (23, "nppdvjthqldpwncqszvftbrmjlhg"),
         (29, "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"),
         (26, "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"),
      ];
      for rec in test_inputs {
         let ans = find_message(rec.1);
         assert_eq!(rec.0, ans);
      }
   }

   #[test]
   // #[ignore]
   fn ans_input_p2() {
      let buf = load_buf_lines("day06/input");
      let ans = day06_p2(&buf);
      println!("day06 p3 {ans}");
      assert_eq!(ans, 1850usize);
   }

   #[test]
   fn ans_test_input() {
      let test_inputs = vec![
         (7, "mjqjpqmgbljsphdztnvjfqwrcgsmlb"),
         (5, "bvwbjplbgvbhsrlpgdmjqwftvncz"),
         (6, "nppdvjthqldpwncqszvftbrmjlhg"),
         (10, "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"),
         (11, "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"),
      ];
      for rec in test_inputs {
         let ans = find_packet(rec.1);
         assert_eq!(rec.0, ans);
      }
   }

   #[test]
   // #[ignore]
   fn ans_input() {
      let buf = load_buf_lines("day06/input");
      let ans = day06(&buf);
      println!("day06 p1 {ans}");
      assert_eq!(ans, 1850usize);
   }

   #[test]
   fn fn_is_start_of_packet() {
      let recs = vec![
         (false, "aaab"),
         (true,  "abcd"),
      ];
      for rec in recs {
         assert_eq!(rec.0, is_start_of_packet(rec.1.as_bytes()));
      }
   }
}
