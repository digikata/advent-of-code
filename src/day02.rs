//! day02.rs
use anyhow::anyhow;
use anyhow::Result;
use text_io::scan;

#[derive(Debug, PartialEq, Clone)]
enum Play {
    Rock,
    Scissors,
    Paper,
}
use Play::{Paper, Rock, Scissors};

enum Outcome {
    Win,
    Lose,
    Draw,
}
use Outcome::{Draw, Lose, Win};

// does player b win lose or draw
fn play(a: &Play, b: &Play) -> Outcome {
    match (a, b) {
        (Rock, Rock) => Outcome::Draw,
        (Rock, Scissors) => Outcome::Lose,
        (Rock, Paper) => Outcome::Win,

        (Scissors, Rock) => Outcome::Win,
        (Scissors, Scissors) => Outcome::Draw,
        (Scissors, Paper) => Outcome::Lose,

        (Paper, Rock) => Outcome::Lose,
        (Paper, Scissors) => Outcome::Win,
        (Paper, Paper) => Outcome::Draw,
    }
}

fn score(out: &Outcome, b: &Play) -> u64 {
    let mut score = 0;
    score += match b {
        Rock => 1,
        Paper => 2,
        Scissors => 3,
    };
    score += match out {
        Win => 6,
        Lose => 0,
        Draw => 3,
    };
    score
}

// part1 parse_line
// part2 has a different strategy
fn parse_line(line: &str) -> Result<(Play, Play)> {
    let (sa, sb): (String, String);
    scan!(line.bytes() => "{} {}", sa, sb);
    let a = match sa.as_str() {
        "A" => Rock,
        "B" => Paper,
        "C" => Scissors,
        _ => {
            return Err(anyhow!("Unknown A {}", sa));
        }
    };
    let b = match sb.as_str() {
        "X" => Rock,
        "Y" => Paper,
        "Z" => Scissors,
        _ => {
            return Err(anyhow!("Unknown B {}", sb));
        }
    };
    let pair = (a, b);
    Ok(pair)
}

// X = try lose
// Y = try draw
// Z = try win
fn parse_line_p2(line: &str) -> Result<(Play, Outcome)> {
    let (sa, sb): (String, String);
    scan!(line.bytes() => "{} {}", sa, sb);
    let a = match sa.as_str() {
        "A" => Rock,
        "B" => Paper,
        "C" => Scissors,
        _ => {
            return Err(anyhow!("Unknown A {}", sa));
        }
    };
    let b = match sb.as_str() {
        "X" => Lose,
        "Y" => Draw,
        "Z" => Win,
        _ => {
            return Err(anyhow!("Unknown B {}", sb));
        }
    };
    let pair = (a, b);
    Ok(pair)
}

/// given play a, and desired outcome out, return play strategy
fn strategy_p2(out: &Outcome, a: &Play) -> Play {
    match (out, a) {
        (Win, Rock) => Paper,
        (Win, Paper) => Scissors,
        (Win, Scissors) => Rock,

        (Lose, Rock) => Scissors,
        (Lose, Paper) => Rock,
        (Lose, Scissors) => Paper,

        (Draw, a) => a.clone(),
    }
}

fn score_line(line: &str) -> Result<u64> {
    let (a, b) = parse_line(line)?;
    let out = play(&a, &b);
    let score = score(&out, &b);
    Ok(score)
}
fn score_line_p2(line: &str) -> Result<u64> {
    let (a, out) = parse_line_p2(line)?;
    let b = strategy_p2(&out, &a);
    let score = score(&out, &b);
    Ok(score)
}

pub fn day02_p1(input: &str) -> Result<u64> {
    let mut score = 0;
    for line in input.split('\n') {
        if line.is_empty() {
            continue;
        }
        score += score_line(line)?;
    }
    Ok(score)
}

pub fn day02_p2(input: &str) -> Result<u64> {
    let mut score = 0;
    for line in input.split('\n') {
        if line.is_empty() {
            continue;
        }
        score += score_line_p2(line)?;
    }
    Ok(score)
}

#[test]
fn t_day02_p1_ans() {
    use crate::load_buf;
    let buf = load_buf("day02/input");
    let score = day02_p1(&buf).unwrap();
    println!("Day02 P1 Ans: {score}");
}

#[test]
fn t_day02_p2_ans() {
    use crate::load_buf;
    let buf = load_buf("day02/input");
    let score = day02_p2(&buf).unwrap();
    println!("Day02 P2 Ans: {score}");
}

#[test]
fn t_day02() {
    use crate::load_buf;
    let buf = load_buf("day02/test_input");
    day02_p1(&buf).unwrap();
}

#[test]
fn t_day02_score_line_p2() -> Result<()> {
    assert_eq!(score_line_p2("A Y\n")?, 4u64);
    assert_eq!(score_line_p2("B X\n")?, 1u64);
    assert_eq!(score_line_p2("C Z\n")?, 7u64);
    Ok(())
}

#[test]
fn t_day02_score_line() -> Result<()> {
    assert_eq!(score_line("A Y\n")?, 8u64);
    assert_eq!(score_line("B X\n")?, 1u64);
    assert_eq!(score_line("C Z\n")?, 6u64);
    Ok(())
}

#[test]
fn t_day02_parse() -> Result<()> {
    let (a, b) = parse_line("A Y\n")?;
    assert_eq!(a, Play::Rock);
    assert_eq!(b, Play::Paper);
    Ok(())
}
