//! dayxx.rs

use crate::load_buf_lines;

pub fn dayxx(_input: &[String]) -> u64 {
   42u64
}

fn parse_line(line: &str) {

}

#[cfg(test)]
mod t {
   use crate::load_buf_lines;
   use super::*;

   #[test]
   fn ans_test_input() {
      let buf = load_buf_lines("dayxx/test_input");
      let ans = dayxx(&buf);
      assert_eq!(ans, 0u64);
   }

   #[test]
   #[ignore]
   fn ans_input() {
      let buf = load_buf_lines("dayxx/input");
      let ans = dayxx(&buf);
      assert_eq!(ans, 0u64);
   }
}
