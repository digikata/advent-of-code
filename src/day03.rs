//! day03.rs

pub fn day03_p1(input: &[String]) -> u64 {
    input
        .iter()
        .map(|line| {
            let ruck = parse_line(line);
            ruck.common().chars().fold(0u64, |score, ch| {
                // let val = score + priority(ch);
                // dbg!(&ch, priority(ch), val);
                score + priority(ch)
            })
        })
        .sum()
}

pub fn day03_p2(input: &[String]) -> u64 {
    let team = chunker(input, 3);
    team.iter()
        .map(|team| {
            let a = team[0];
            let b = team[1];
            let c = team[2];
            let com2 = common_two(a, b);
            let badge = common_two(&com2, c);
            let badge_ch = badge.chars().next().unwrap();
            priority(badge_ch)
        })
        .sum()
}

fn chunker(input: &[String], n: usize) -> Vec<Vec<&str>> {
    let mut i = 0usize;
    let (tv, _cv) = input.iter().fold((Vec::new(), Vec::new()), |vv, line| {
        let (mut tv, mut chunkv) = vv;
        chunkv.push(line.as_str());
        i += 1;
        if i == n {
            i = 0;
            tv.push(chunkv);
            (tv, Vec::new())
        } else {
            (tv, chunkv)
        }
    });
    tv
}

#[derive(Default)]
struct Rucksack {
    front: String,
    back: String,
}

impl Rucksack {
    fn common(&self) -> String {
        common_two(&self.front, &self.back)
    }
}

fn common_two(a: &str, b: &str) -> String {
    use std::collections::HashSet as Set;
    // overkill but easy
    let hf: Set<char> = Set::from_iter(a.chars());
    let hb: Set<char> = Set::from_iter(b.chars());
    hf.intersection(&hb).fold(String::new(), |mut out, item| {
        out.push(*item);
        out
    })
}

fn parse_line(line: &str) -> Rucksack {
    let h = line.len() / 2;

    Rucksack {
        front: String::from(&line[0..h]),
        back: String::from(&line[h..]),
    }
}

fn priority(item: char) -> u64 {
    if ('a'..='z').contains(&item) {
        return 1u64 + (item as u64 - 'a' as u64);
    } else if ('A'..='Z').contains(&item) {
        return 27u64 + (item as u64 - 'A' as u64);
    }
    0u64
}

#[cfg(test)]
mod t {
    use super::*;
    use crate::load_buf_lines;

    #[test]
    fn test_input_p2() {
        let buf = load_buf_lines("day03/test_input");
        let score = day03_p2(&buf);
        println!("day03 p2 test {score}");
        assert_eq!(score, 70);
    }

    #[test]
    fn ans_input_p2() {
        let buf = load_buf_lines("day03/input");
        let score = day03_p2(&buf);
        println!("day03 p2 ans {score}");
    }

    #[test]
    fn test_input() {
        let buf = load_buf_lines("day03/test_input");
        let score = day03_p1(&buf);
        println!("day03 p1 test {score}");
        assert_eq!(score, 157);
    }

    #[test]
    fn ans_input() {
        let buf = load_buf_lines("day03/input");
        let score = day03_p1(&buf);
        println!("day03 p1 ans {score}");
    }

    fn get_test_ruck() -> Rucksack {
        let buf = load_buf_lines("day03/test_input");
        let mut itr = buf.iter();
        parse_line(itr.next().unwrap())
    }

    #[test]
    fn common() {
        let ruck = get_test_ruck();
        assert_eq!(ruck.common(), "p");
    }

    #[test]
    fn priority_fn() {
        assert_eq!(priority('a'), 1);
        assert_eq!(priority('z'), 26);
        assert_eq!(priority('A'), 27);
        assert_eq!(priority('Z'), 52);
    }

    #[test]
    fn t_parse_line() {
        let ruck = get_test_ruck();
        assert_eq!(ruck.front, "vJrwpWtwJgWr");
        assert_eq!(ruck.back, "hcsFMMfFFhFp");
    }
}
