//! day01.rs

#[derive(Clone)]
struct Elf {
    cals: u64,
}

pub fn day01(input: &str) {
    let lbuf = input.split('\n');

    let mut current_elf = Elf { cals: 0 };
    let mut elves = Vec::new();
    for line in lbuf {
        if line == " " || line.is_empty() {
            elves.push(current_elf.clone());
            current_elf.cals = 0u64;
        } else {
            let line_cals: u64 = line.parse::<u64>().expect("bad parse");
            current_elf.cals += line_cals;
        }
    }
    elves.sort_by(|a, b| a.cals.partial_cmp(&b.cals).unwrap());

    println!("max elf: {}", elves.last().unwrap().cals);
    let sum3: u64 = elves.iter().rev().take(3).map(|e| e.cals).sum();
    println!("3 elf: {}", sum3);
}

#[test]
fn t_day01() {
    use crate::load_buf;
    let buf = load_buf("day01/test_input");
    day01(&buf);
}

#[test]
fn t_day01_ans() {
    use crate::load_buf;
    let buf = load_buf("day01/input");
    day01(&buf);
}
