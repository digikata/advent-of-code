//! day05.rs
use text_io::scan;

// use crate::load_buf_lines;

pub fn day05(input: &[String], n_stacks: usize) -> String {
   let mut stacks = Stacks::new(n_stacks);
   for line in input {
      match parse_line_kind(line) {
         LineKind::Stack => {
            let stack_bits = parse_stack_line(line);
            for (i, item) in stack_bits {
               let iz = i - 1;
               stacks.s[iz].push_bottom(item);
            }
            // println!("{}", stacks.top_of_stacks());
         },
         LineKind::Move => {
            let mv = parse_move(line);
            stacks.apply_move(&mv);
            // println!("{} after mv {:?}", stacks.top_of_stacks(), &mv);
         }
         _ => {}
      }
   }
   // dbg!(&stacks);
   stacks.top_of_stacks()
}

pub fn day05_p2(input: &[String], n_stacks: usize) -> String {
   let mut stacks = Stacks::new(n_stacks);
   for line in input {
      match parse_line_kind(line) {
         LineKind::Stack => {
            let stack_bits = parse_stack_line(line);
            for (i, item) in stack_bits {
               let iz = i - 1;
               stacks.s[iz].push_bottom(item);
            }
            // println!("{}", stacks.top_of_stacks());
         },
         LineKind::Move => {
            let mv = parse_move(line);
            stacks.apply_move_p2(&mv);
            // println!("{} after mv {:?}", stacks.top_of_stacks(), &mv);
         }
         _ => {}
      }
   }
   // dbg!(&stacks);
   stacks.top_of_stacks()
}

/// move instruction
#[derive(Debug)]
struct Move {
   num: usize,
   from: usize,
   to: usize,
}

#[derive(Debug)]
struct Stack {
   v: Vec<char>
}

impl Stack {
   /// top of the stack is end of the vec
   fn pop(&mut self) -> char {
      //self.v.remove(0)
      self.v.pop().unwrap()
   }

   fn stack(&mut self, item: char) {
      self.v.push(item)
   }

   // for parsing
   fn push_bottom(&mut self, item: char) {
      self.v.insert(0, item)
   }
}


#[derive(Debug)]
struct Stacks {
   s: Vec<Stack>
}

impl Stacks {
   pub fn new(n_stacks: usize) -> Self {
      let mut s = Vec::<Stack>::with_capacity(n_stacks);
      for _ in 0..n_stacks {
         s.push(Stack { v: Vec::new() });
      }
      Stacks { s }
   }

   /// Cratemover 9000 part 1
   pub fn apply_move(&mut self, cmd: &Move) {
      let ifrom = (cmd.from - 1) as usize;
      let ito   = (cmd.to - 1) as usize;
      for _ in 0..cmd.num {
         let item = self.s[ifrom].pop();
         self.s[ito].stack(item);
      }
   }

   /// Cratemover 9001 part 2
   pub fn apply_move_p2(&mut self, cmd: &Move) {
      let ifrom = (cmd.from - 1) as usize;
      let ito   = (cmd.to - 1) as usize;

      let mut in_motion = Vec::new();
      for _ in 0..cmd.num {
         in_motion.push(self.s[ifrom].pop());
      }
      for item in in_motion.iter().rev() {
         self.s[ito].stack(*item);
      }
   }

   pub fn top_of_stacks(&self) -> String {
      self.s.iter().fold(String::new(), |mut out, stack| {
         if stack.v.is_empty() {
            out.push('-');
         } else {
            let item = stack.v.last().unwrap();
            out.push(*item);
         }
         out
      })
   }
}

#[derive(Debug, PartialEq)]
enum LineKind {
   Blank,
   Stack,
   StackIndex,
   Move
}

fn parse_line_kind(line: &str) -> LineKind {
   if line.is_empty() || line.len() < 3 {
      return LineKind::Blank;
   }

   let (tok, _) = line.split_at(3);
   if tok.contains('[') || tok == "   " {
      return LineKind::Stack;
   }
   if tok.contains("mov") {
      return LineKind::Move
   }
   LineKind::StackIndex
}

fn parse_move(line: &str) -> Move {
   let num: usize;
   let to: usize;
   let from: usize;
   scan!(line.bytes() => "move {} from {} to {}", num, from, to);
   Move {
      num,
      to,
      from,
   }
}


/// break into fixed strs of n (or smaller for the last chunk)
/// split_at only works because the aoc inputs are clean char at byte chunks
fn col_vec(line: &str, n: usize) -> Vec<&str> {
   let mut cursor = line;
   let mut v = Vec::new();
   while cursor.len() >= n {
      let (tok, new_cursor) = cursor.split_at(n);
      v.push(tok);
      cursor = new_cursor;
   }
   if !cursor.is_empty() {
      v.push(cursor);
   }
   v
}

fn parse_stack_line(line: &str) -> Vec<(usize, char)> {
   let cols = col_vec(line, 4);
   cols.iter().enumerate()
      .filter(|(_, col)| !col.trim().is_empty())
      .map(|(i, col)| {
         let item: char;
         scan!(col.bytes() => "[{}]", item);
         (i + 1, item)
      })
      .collect()
   // Vec::new()
}

#[cfg(test)]
mod t {
   use crate::load_buf_lines;
   use super::*;

   #[test]
   fn ans_test_input_p2() {
      let buf = load_buf_lines("day05/test_input");
      let ans = day05_p2(&buf, 3);
      assert_eq!(&ans, "MCD");
   }

   #[test]
   fn ans_input_p2() {
      let buf = load_buf_lines("day05/input");
      let ans = day05_p2(&buf, 9);
      assert_eq!(&ans, "PRTTGRFPB");
      println!("day05 ans p2 {}", &ans);
   }

   #[test]
   fn ans_test_input() {
      let buf = load_buf_lines("day05/test_input");
      let ans = day05(&buf, 3);
      assert_eq!(&ans, "CMZ");
   }

   #[test]
   fn ans_input() {
      let buf = load_buf_lines("day05/input");
      let ans = day05(&buf, 9);
      assert_eq!(&ans, "ZRLJGSCTR");
      println!("day05 ans p1 {}", &ans);
   }

   #[test]
   fn fn_parse_stack_line() {
      let sl = parse_stack_line("    [D]");
      assert_eq!(vec![(2, 'D')], sl);

      let sl = parse_stack_line("[N] [C]");
      assert_eq!(vec![(1, 'N'), (2, 'C')], sl);
   }

   #[test]
   // #[ignore]
   fn fn_col_vec() {
      let sample = "111122223333";
      let cols = col_vec(sample, 4);
      assert_eq!(vec!["1111", "2222", "3333"], cols);
      // let mut cursor = sample;
      // let mut v = Vec::new();
      // while cursor.len() >= 4 {
      //    let (tok, new_cursor) = cursor.split_at(4);
      //    // dbg!(tok);
      //    v.push(tok);
      //    cursor = new_cursor;
      // }
      // dbg!(v);
   }

   #[test]
   fn fn_parse_line_kind() {
      let lk = parse_line_kind("    [D]");
      assert_eq!(LineKind::Stack, lk);

      let lk = parse_line_kind("[A]");
      assert_eq!(LineKind::Stack, lk);

      let lk = parse_line_kind("");
      assert_eq!(LineKind::Blank, lk);

      let lk = parse_line_kind("move 4 from 2 to 1");
      assert_eq!(LineKind::Move, lk);
   }
}
