//! day04.rs
use text_io::scan;

pub fn day04(input: &[String]) -> usize {
    input
        .iter()
        .map(|line| parse_line(line))
        .filter(|(a, b)| full_containment(a, b))
        .count()
}

pub fn day04_p2(input: &[String]) -> usize {
    input
        .iter()
        .map(|line| parse_line(line))
        .filter(|(a, b)| overlap(a, b))
        .count()
}

fn parse_line(line: &str) -> (SecRange, SecRange) {
    let a: u64;
    let b: u64;
    let c: u64;
    let d: u64;
    scan!(line.bytes() => "{}-{},{}-{}", a, b, c, d);
    let sa = SecRange::new(a, b);
    let sb = SecRange::new(c, d);
    (sa, sb)
}

fn full_containment(a: &SecRange, b: &SecRange) -> bool {
    a.inside(b) || b.inside(a)
}

fn overlap(a: &SecRange, b: &SecRange) -> bool {
    a.overlaps(b) || b.overlaps(a)
}

#[derive(Default, PartialEq, Debug)]
struct SecRange {
    start: u64,
    end: u64,
}

impl SecRange {
    fn new(start: u64, end: u64) -> SecRange {
        SecRange { start, end }
    }

    fn contains(&self, s: u64) -> bool {
        (s >= self.start) && (s <= self.end)
    }

    fn inside(&self, b: &SecRange) -> bool {
        b.contains(self.start) && b.contains(self.end)
    }

    fn overlaps(&self, b: &SecRange) -> bool {
        b.contains(self.start) || b.contains(self.end)
    }
}

#[cfg(test)]
mod t {
    use super::*;
    use crate::load_buf_lines;

    #[test]
    fn ans_test_input_p1() {
        let buf = load_buf_lines("day04/test_input");
        let ans = day04(&buf);
        assert_eq!(ans, 2usize);
    }

    #[test]
    fn ans_test_input_p2() {
        let buf = load_buf_lines("day04/test_input");
        let ans = day04_p2(&buf);
        assert_eq!(ans, 4usize);
    }

    #[test]
    fn ans_input_p1() {
        let buf = load_buf_lines("day04/input");
        let ans = day04(&buf);
        println!("day04 p1 {ans}");
    }

    #[test]
    fn ans_input_p2() {
        let buf = load_buf_lines("day04/input");
        let ans = day04_p2(&buf);
        println!("day04 p2 {ans}");
    }

    #[test]
    fn fn_parse_line() {
        let (a, b) = parse_line("2-4,6-8");
        assert_eq!(SecRange::new(2, 4), a);
        assert_eq!(SecRange::new(6, 8), b);
    }
}
