mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;

pub use crate::day01::*;
pub use crate::day02::*;
pub use crate::day03::*;
pub use crate::day04::*;
pub use crate::day05::*;
pub use crate::day06::*;

pub fn load_buf(input: &str) -> String {
    let msg = format!("error loading {input}");
    std::fs::read_to_string(input).expect(&msg)
}

pub fn load_buf_lines(input: &str) -> Vec<String> {
    let buf = load_buf(input);
    buf.split('\n').map(String::from).collect()
}
